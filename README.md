# pyo3-setup [![Crates.io Badge]](https://crates.io/crates/pyo3-setup) [![License Badge]](#license)

Quickly up a Rust PyO3 project with setuptools-rust.

## Installation

pyo3-setup is installed with Cargo.

### From crates.io

```sh
$ cargo install pyo3-setup --locked
```

The executable will then be available to run anywhere.

### Locally

You can also install from a cloned repository:

```sh
$ cargo install --path .
```

## Use

To set up a new project, run `pyo3-setup path/to/project`.
The project name will be inferred from the last folder in the path.

More information is available in the help menu with `pyo3-setup --help`:

```text
USAGE:
    pyo3-setup [FLAGS] [OPTIONS] <PATH>

FLAGS:
    -h, --help               Prints help information
        --non-interactive    Don't require user input to confirm before using default values
    -V, --version            Prints version information

OPTIONS:
        --author <NAME>                        The author name to use. Defaults to name configured via Git
        --email <EMAIL>                        The author email to use. Defaults to email configured via Git
        --desc <DESCRIPTION>                   The description of the project
    -n, --name <NAME>                          The name of the project. Inferred from the path by default
        --pyo3-version <VERSION>               The version of PyO3 to use. Defaults to the latest version
        --setuptools-rust-version <VERSION>    The version of setuptools-rust to use [default: >=1]
        --setuptools-version <VERSION>         The version of setuptools to use [default: >=59]
        --package-version <VERSION>            The version of the package to set

ARGS:
    <PATH>    The path to the project
```

## License

This project is licensed under the GNU General Public License, Version 3.0
([LICENSE](LICENSE) or <https://www.gnu.org/licenses/gpl-3.0.en.html>).
However, any generated files can be modified to your liking
and licensed under whatever license you choose.

[crates.io badge]: https://img.shields.io/crates/v/pyo3-setup
[license badge]: https://img.shields.io/crates/l/pyo3-setup
