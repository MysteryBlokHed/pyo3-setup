//! Functions for the Clap CLI
use clap::{App, Arg};

/// Get the Clap app for pyo3-setup
pub(crate) fn get_app<'a>() -> App<'a, 'a> {
    App::new("PyO3 Setup")
        .setting(clap::AppSettings::SubcommandsNegateReqs)
        .version("0.1.1")
        .author("Adam Thompson-Sharpe")
        .about("Set up a Rust PyO3 project with setuptools-rust")
        .arg(
            Arg::with_name("path")
                .help("The path to the project")
                .required(true)
                .value_name("PATH"),
        )
        .arg(
            Arg::with_name("author")
                .help("The author name to use. Defaults to name configured via Git")
                .long("author")
                .value_name("NAME"),
        )
        .arg(
            Arg::with_name("author_email")
                .help("The author email to use. Defaults to email configured via Git")
                .long("email")
                .value_name("EMAIL"),
        )
        .arg(
            Arg::with_name("name")
                .help("The name of the project. Inferred from the path by default")
                .short("n")
                .long("name")
                .value_name("NAME"),
        )
        .arg(
            Arg::with_name("description")
                .help("The description of the project")
                .long("desc")
                .value_name("DESCRIPTION"),
        )
        .arg(
            Arg::with_name("version")
                .help("The version of the package to set")
                .long("package-version")
                .value_name("VERSION"),
        )
        .arg(
            Arg::with_name("non_interactive")
                .help("Don't require user input to confirm before using default values")
                .long("non-interactive"),
        )
        .arg(
            Arg::with_name("pyo3_version")
                .help("The version of PyO3 to use. Defaults to the latest version")
                .long("pyo3-version")
                .value_name("VERSION"),
        )
        .arg(
            Arg::with_name("setuptools_version")
                .help("The version of setuptools to use")
                .long("setuptools-version")
                .default_value(">=59")
                .value_name("VERSION"),
        )
        .arg(
            Arg::with_name("setuptools_rust_version")
                .help("The version of setuptools-rust to use")
                .long("setuptools-rust-version")
                .default_value(">=1")
                .value_name("VERSION"),
        )
}
