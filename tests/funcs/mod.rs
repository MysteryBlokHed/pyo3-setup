//! Functions used in multiple tests
use std::{
    fs,
    path::{Path, PathBuf},
    process::{Command, Output},
};

pub fn run_cargo(program_args: &[&str], path: Option<&Path>) -> Output {
    let cargo_toml_dir = format!("{}/Cargo.toml", env!("CARGO_MANIFEST_DIR"));
    let mut args = vec!["run", "--manifest-path", &cargo_toml_dir];
    args.extend_from_slice(program_args);

    run_with_args("cargo", &args, path)
}

pub fn run_with_args(cmd: &str, args: &[&str], path: Option<&Path>) -> Output {
    if let Some(p) = path {
        Command::new(cmd)
            .args(args)
            .current_dir(p)
            .output()
            .expect("failed to execute process")
    } else {
        Command::new(cmd)
            .args(args)
            .output()
            .expect("failed to execute process")
    }
}

/// Check if a list of files exist
///
/// # Arguments
///
/// - `files` - A list of paths to files
/// - `print_prefix` - A prefix for the message after assertion
pub fn check_files_exist<P: AsRef<Path>>(base_path: P, files: &[&str], print_prefix: &str) {
    let mut base = PathBuf::from(base_path.as_ref());

    let initial_len = base.components().count();

    for file in files.iter() {
        base.push(file);
        let new_len = base.components().count();
        println!("{}: Checking file {}", print_prefix, base.display());
        assert!(fs::metadata(&base).is_ok());
        println!("{}: File {} exists (and should)", print_prefix, file);

        for _ in initial_len..new_len {
            base.pop();
        }
    }
}
